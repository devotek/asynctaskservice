package sharp.teknos.dev.asynctaskeg;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ExampleActivity extends AppCompatActivity {

    private TextView countTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);
        countTextView = (TextView)findViewById(R.id.countText);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Util.ACTION);
        registerReceiver(myReceiver,intentFilter);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver);
    }

    public void onStartPressed(View view){
        Intent intent = new Intent(this,MyService.class);
        startService(intent);
    }

    public void onStopPressed(View view){
        Intent intent = new Intent(this,MyService.class);
        stopService(intent);
    }

    private BroadcastReceiver myReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            int count = bundle.getInt(Util.COUNT);
            countTextView.setText(Integer.toString(count));
        }

    };
}
