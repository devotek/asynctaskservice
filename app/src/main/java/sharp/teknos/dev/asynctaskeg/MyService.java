package sharp.teknos.dev.asynctaskeg;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service implements Runnable{
    private Handler handler = new Handler();
    private int counter = 0;
    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.w("SRV","Started");
        handler.postDelayed(this,1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.w("SRV","Ended");
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        Log.w("SRV",counter+"");
        counter = counter+1;
        Intent intent = new Intent();
        intent.putExtra(Util.COUNT,counter);
        intent.setAction(Util.ACTION);
        sendBroadcast(intent);
        handler.postDelayed(this,1000);
    }
}
