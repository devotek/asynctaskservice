package sharp.teknos.dev.asynctaskeg;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private String[] addresses = new String[0];
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        enterNames("Ram","jay","shiva","Rajesh");
        listView = (ListView)findViewById(R.id.listView);
        //arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,addresses);
        //listView.setAdapter(arrayAdapter);
        ConnectServer connectServer =new ConnectServer();
        connectServer.execute("https://maps.googleapis.com/maps/api/place/textsearch/json?query=ITPB+Bangalore&key=AIzaSyCDo_lPCPKaBFCcouU6L0s6geswaNdC30I");

    }

    public void enterNames(String... names){
        for (int i = 0; i < names.length; i++) {

        }
    }

    private class ConnectServer extends AsyncTask<String,Void,String>{


        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this,"Wait","Connecting server");
        }

        @Override
        protected String doInBackground(String... urls) {

            StringBuilder stringBuilder = new StringBuilder();
            try {
                URL url = new URL(urls[0]);

                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
                httpsURLConnection.setReadTimeout(15000);
                httpsURLConnection.setConnectTimeout(20000);
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.setDoInput(true);

                httpsURLConnection.connect();

                InputStream inputStream = httpsURLConnection.getInputStream();

                InputStreamReader inputStreamReader = new InputStreamReader(inputStream,"UTF-8");

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line = "";

                while ((line = bufferedReader.readLine()) != null){
                    stringBuilder.append(line).append("\n");
                }

                inputStreamReader.close();
                inputStream.close();
                bufferedReader.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            //HttpsURLConnection httpsURLConnection =



            return stringBuilder.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.w("AsyncTask",s);

            try {
                JSONObject jsonObject = new JSONObject(s);
                if (jsonObject.getString("status").equals("OK")){
                    //
                    JSONArray resultArray = jsonObject.getJSONArray("results");
                    addresses = new String[resultArray.length()];
                    for (int i = 0; i < resultArray.length(); i++) {
                        addresses[i] = resultArray.getJSONObject(i).getString("formatted_address");
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,addresses);
                    listView.setAdapter(arrayAdapter);

                }
                progressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
